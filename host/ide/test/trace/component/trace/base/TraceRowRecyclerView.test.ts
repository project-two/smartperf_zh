/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// @ts-ignore
import {TraceRowRecyclerView} from "../../../../../dist/trace/component/trace/base/TraceRowRecyclerView.js";

describe("TraceRow Test", () => {
    beforeAll(() => {
    })

    it('Utils Test01', () => {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow)
    });

    it('Test02', function () {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow.dataSource).toBeTruthy();
    });

    it('Test03', function () {
        let traceRow = new TraceRowRecyclerView();
        traceRow.dataSource=false
        expect(traceRow.dataSource).toBeTruthy();
    });

    it('Test04', function () {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow.renderType).toBeTruthy();
    });

    it('Test05', function () {
        let traceRow = new TraceRowRecyclerView();
        traceRow.renderType=false
        expect(traceRow.renderType).toBeFalsy();
    });

    it('Test06', function () {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow.refreshRow()).toBeUndefined();
    });

    it('Test07', function () {
        let traceRow = new TraceRowRecyclerView();
        traceRow.dataSource = jest.fn(()=>true)
        traceRow.dataSource.filter = jest.fn(()=>true)
        expect(traceRow.measureHeight()).toBeUndefined();
    });

    it('Test08', function () {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow.initUI()).toBeUndefined();
    });
    it('Test09', function () {
        let traceRow = new TraceRowRecyclerView();
        expect(traceRow.initUI()).toBeUndefined();
    });
})